module.exports = {
  apps: [
    {
      name: 'NuxtAppName',
      exec_mode: 'cluster',
      instances: 'max', // Or a number of instancescluster
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start',
      env_production: {
      NODE_ENV: "production",
    }
    }
  ]
}