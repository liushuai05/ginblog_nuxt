import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = () => new Vuex.Store({
    state: {
        icp_record: ''
    },
    mutations: {
        save(state,icp_record) {
            state.icp_record=icp_record
        },
        save_profileInfo(state,profileInfo) {
            state.profileInfo=profileInfo
        },
        add_cateList(state,cateList) {
            state.cateList=cateList
        },
        save_login_dialog(state,login_dialog) {
            state.login_dialog=login_dialog
        },
        save_login_user(state,login_user) {
            state.username=login_user.username
            state.id=login_user.user_id
        }
    }
})

export default store