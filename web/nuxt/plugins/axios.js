
import Vue from 'vue'
// import moment from 'moment'


export default function (app) {
  let axios = app.$axios;
  axios.defaults.baseURL = 'http://localhost:3000/api/v1'
  // 基本配置
  axios.defaults.timeout = 10000;
  axios.defaults.method = 'post';


  // 请求回调
  axios.onRequest(config => { })

  // 返回回调
  axios.onResponse(res => { })

  // 错误回调
  axios.onError(error => { })

  Vue.prototype.$http = axios
};

Vue.filter('dateformat', function (indate, outdate) {

  let ret;

  switch (typeof indate) {
    case "string":
      indate = new Date(indate);
      break;
    case "number":
      indate = new Date(indate);
      break;
  }
  const opt = {
    "Y+": indate.getFullYear().toString(),        // 年
    "M+": (indate.getMonth() + 1).toString(),     // 月
    "D+": indate.getDate().toString(),            // 日
    "HH": indate.getHours().toString(),           // 时
    "MM": indate.getMinutes().toString(),         // 分
    "SS": indate.getSeconds().toString()          // 秒
    // 有其他格式化字符需求可以继续添加，必须转化成字符串
  };
  for (let k in opt) {
    ret = new RegExp("(" + k + ")").exec(outdate);
    if (ret) {
      outdate = outdate.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
    };
  };
  return outdate;

  // return moment(indate).format(outdate)
})

