/*
 * @Author: 偻儸小卒[EdisonLiu_]
 * @Date: 2021-03-19 16:50:13
 * @LastEditors: 偻儸小卒[EdisonLiu_]
 * @LastEditTime: 2021-03-19 18:26:45
 * @Description: 
 */
import axios from "axios"

axios.defaults.baseURL = 'http://localhost:3000/api/v1'
axios.defaults.baseURL = 'https://blog.liushuai.cq.cn/api/v1/'
function dateformat(indate, outdate) {

    let ret;

    switch (typeof indate) {
        case "string":
            indate = new Date(indate);
            break;
        case "number":
            indate = new Date(indate);
            break;
    }
    const opt = {
        "Y+": indate.getFullYear().toString(),        // 年
        "M+": (indate.getMonth() + 1).toString(),     // 月
        "D+": indate.getDate().toString(),            // 日
        "HH": indate.getHours().toString(),           // 时
        "MM": indate.getMinutes().toString(),         // 分
        "SS": indate.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(outdate);
        if (ret) {
            outdate = outdate.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        };
    };
    return outdate;

    // return moment(indate).format(outdate)
}


const sitemap = {
    path: '/sitemap.xml', // 生成的文件路径
    hostname: 'https://blog.liushuai.cq.cn/', // 网址
    cacheTime: 1000 * 60 * 60 * 24, // 1天 更新频率，只在 generate: false有用
    gzip: true, // 生成 .xml.gz 压缩的 sitemap
    generate: false, // 允许使用 nuxt generate 生成
    // 排除不要页面
    exclude: [
        '/404' //  这里的路径相对 hostname
    ],
    // xml默认的配置
    defaults: {
        changefreq: 'always',
        // lastmod: new Date()
        lastmod: dateformat(new Date(), "YYYY-MM-DD"),
    },
    // 需要生成的xml数据, return 返回需要给出的xml数据
    routes: async () => {
        console.log(axios.defaults.baseURL);
        // // 从后台获取数据,拼接url生成更多的xml数据
        const { data: CategoryData } = await axios.get("category")
        const { data: ArticleData } = await axios.get("article")

        var routes = [
            {
                url: "/",  //  这里的路径相对 hostname
                changefreq: "always",
                lastmod: dateformat(new Date(), "YYYY-MM-DD"),
                // lastmod: new Date()
                priority:"1"

            },
            {
                url: "/search",
                changefreq: "always",
                lastmod: dateformat(new Date(), "YYYY-MM-DD"),
                // lastmod:  new Date()
                priority:"0.89"
            }
        ]
        // 分类
        if (CategoryData && CategoryData.data) {
            var CategoryArr = [];
            CategoryData.data.forEach(item => {
                routes.push({
                    url: "/category/" + item.id,
                    lastmod: dateformat(new Date(), "YYYY-MM-DD"),
                    // lastmod:  new Date(),
                    changefreq: "yearly",
                    priority:"0.88"

                })
            });
        }
        // //文章
        if (ArticleData && ArticleData.data) {
            var ArticleArr = [];
            ArticleData.data.forEach(item => {
                var creatAt = new Date(item.CreatedAt);
                var updateAt = new Date(item.UpdatedAt);
                var lastmod = creatAt > updateAt ? item.CreatedAt : item.UpdatedAt
                routes.push({
                    url: "/article/detail/" + item.ID,
                    lastmod: dateformat(lastmod, "YYYY-MM-DD"),
                    // lastmod: lastmod,
                    changefreq: "yearly",
                    priority:"0.85"
                    
                })
            });
        }

        return routes
    }
}

export default sitemap