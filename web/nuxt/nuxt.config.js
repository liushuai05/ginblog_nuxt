import colors from 'vuetify/es5/util/colors'
import sitemap from './plugins/sitemap'
// import cheerio from 'cheerio'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: ' 个人博客-%s ',
    title: '个人博客',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      // <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui"></meta>
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900' },
      // { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css' },
      // { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css' },
    ],
    script: [
      // { src: 'https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js' }
    ],
  },
  server: {
    port: 3008, //使用3008端口启动服务
    host: '0.0.0.0',
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/axios' },
    // { src: '~/plugins/vuetify' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/markdownit'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/vuetify',
    '@nuxtjs/sitemap',
  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    code: true,
    use: [
      // 'markdown-it-div',
      // 'markdown-it-attrs', //代码解析
      // 'markdown-it-highlightjs', //高亮
      // 'markdown-it-prism',
      // 'markdown-it-anchor',
      // 'markdown-it-toc-done-right',
      // 'markdown-it-emoji',
      // 'markdown-it-color'

    ]
  },
  sitemap: sitemap,
  hooks: {
    'render:route': (url, result) => {
      //不知道为什么加上下面的这些就报错
      // this.$ = cheerio.load(result.html, { decodeEntities: false });
      //由于window.__nuxt__总是位于body中的第一个script中，
      //所以我移除了body中第一个脚本标签
      // this.$(`body script`).eq(0).remove();

      // this.$(`meta`).removeAttr('data-n-head');
      // result.html = this.$.html()
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    //css 分离打包
    extractCSS: { allChunks: true },
    //开启分析
    analyze: true,
    assetFilter: function(assetFilename) {	    		
      return assetFilename.endsWith('.js');	    	
    },
    maxChunkSize: 300,
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 1000000,
        chunks: 'all',
        automaticNameDelimiter: '.',
        maxAsyncRequests: 7,
        cacheGroups: {
          vuetify: {
            test: /node_modules[\\/]_vuetify/,
            chunks: 'all',
            priority: 21,
            name: true
          },
          vueCore: {
            test: /node_modules[\\/]_vue/,
            chunks: 'all',
            priority: 20,
            name: true
          },
          markdownIt: {
            test: /node_modules[\\/]_markdown-it/,
            chunks: 'all',
            priority: 22,
            name: true
          },
          // coreJs: {
          //   test: /node_modules[\\/]_core-js/,
          //   chunks: 'all',
          //   priority: 16,
          //   name: true
          // },
        }
      }
    },
    //打包屏蔽
    extend(config, { isDev, isClient }) {
      if (isClient) {
        // config.externals = {
        //   'Vuetify': 'Vuetify'
        // }
      }
    }
  }
}
