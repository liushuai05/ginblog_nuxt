package main

import (
	"ginblog/model"
	"ginblog/routes"
	"ginblog/cmd"
	// "fmt"
	"flag"
)

// type cmdData struct {
// 	username string
// 	pwd string
//  }

func main() {

	//定义几个变量，用于接收命令行的参数值
	var cmdtxt string
	var user string
	var pwd string
	flag.StringVar(&cmdtxt, "cmd", "", "是否修改密码，默认为空")
	flag.StringVar(&user, "u", "", "用户名,默认为空")
	flag.StringVar(&pwd, "pwd", "", "密码,默认为空")
	//这里有一个非常重要的操作,转换， 必须调用该方法
	flag.Parse()
	// $ go run main.go  --cmd changePwd  -u admin -pwd 123456

	if cmdtxt==""{
		// 引用数据库
		model.InitDb()

		// 引入路由组件
		routes.InitRouter()
	}else{
		// 调用 books结构体
		var userData=cmd.CmdData{Username:user,Pwd:pwd}
		//执行cmd命令
		cmd.CmdRun(cmdtxt,&userData)
	}
	

}
